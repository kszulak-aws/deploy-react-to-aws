# Deploy React to AWS

Deploy a sample React application to AWS S3 bucket and CloudFront

React Js is a JavaScript library for building user interfaces (https://reactjs.org/)

Amazon S3 is an object storage built to retrieve any amount of data from anywhere (https://aws.amazon.com/s3/)

Amazon CloudFront helps you securely deliver content with low latency and high transfer speeds (https://aws.amazon.com/cloudfront)

![Deploy code to Web](docs/Deploy-code-to-Web.png)

Prerequisites
- AWS Account
https://aws.amazon.com/free
- Git installed
https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- Node JS installed
https://nodejs.dev/download/


Steps

- [ ] Deploy CloudFormation template
- [ ] Create AWS IAM user
- [ ] Create a new repository and clone it
- [ ] Copy **.gitlab-ci.yml** and **.gitignore** to your repository
- [ ] Create a React app
- [ ] Add credentials to GitLab variables
- [ ] Commit&Push
- [ ] Check your website



1. Deploy CloudFormation template  
Deploy the template file **AWS/s3-cloudfront-iac.yaml** in CloudFormation.  
https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-console-create-stack.html  
This CloudFromation template creates an S3 bucket and a CloudFront distribution where the S3 bucket is the origin.  
Also an IAM policy is created that has write access to the bucket and a invalidate cache permission on the CloudFront distribution.  
The output of the deployment contains information you'll need in Step 6.  

2. Create AWS IAM user  
Create a programmatic IAM user.  
https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html  
Attach the policy created in step 1. to the new user. You can find the name of the policy in the CloudFormation output PolicyName.  
https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_manage-attach-detach.html#add-policies-console  
As last step of the IAM user creation you'll receive an access key and a secret access key. Save them! You'll need them in Step 6.


3. Create a new repository and clone it  
If you don't have a repository yet, create one on GitLab.  
https://docs.gitlab.com/ee/user/project/working_with_projects.html  
Clone the repository to your local environment.


4. Copy **.gitlab-ci.yml** and **.gitignore** to your repository  
By adding **.gitlab-ci.yml** to the root of your repository you enable GitLab CI.  
The file **.gitignore** contains a few folders Git will avoid adding to the repository.


5. Create a React app  
In your repository root run the following command in a command line window:  
``npx create-react-app app``  
This will create a React application in the app folder.  
https://reactjs.org/docs/create-a-new-react-app.html  
https://github.com/facebook/create-react-app


6. Add these settings and credentials to GitLab->Settings->CI/CD->Variables  
```
    AWS_ACCESS_KEY_ID = [from step 2]
    AWS_SECRET_ACCESS_KEY = [from step 2, use the option 'Mask variable' to avoid printing this value in log files]
    AWS_DEFAULT_REGION = [the region where you deployed the template in step 1. (like eu-west-1)]
    CONFIG_DEPLOY_TARGET_CLOUDFRONT = [output CloudFrontDistribution from step 1]
    CONFIG_DEPLOY_TARGET_S3 = [output S3BucketName from step 1]
    CI = false
```
Note: you should use protected variables which will only be usable in protected branches!  
https://docs.gitlab.com/ee/user/project/protected_branches.html


7. Commit&Push  
You'll see the progress on GitLab in CI/CD->Pipelines

8. Check your website
Visit the URL listed in the CloudFormation deployment's output as CloudFrontUrl in step 1. 


From this point on everytime you push modifications of the React app to GitLab, the depolyment will automatically happen.
